package org.deuxpiedsdeuxroues.velobs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.util.TypedValue;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.content.pm.PackageManager;

public class MainActivity extends ActionBarActivity {

    boolean hasMail = false ;
    final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        if (BuildConfig.DEBUG){
            Log.d("MAINACTIVITY", "Entrée dans MainActivity");
        }
        Display display = getWindowManager().getDefaultDisplay();

        int height = 0 ;

            height = display.getHeight();


        if ((height>0)&&(height<801)) {

            TextView tvmail = (TextView) findViewById(R.id.mailtv) ;
            tvmail.setTextSize(TypedValue.COMPLEX_UNIT_DIP,14);
            TextView tvcall = (TextView) findViewById(R.id.textCallService) ;
            tvcall.setTextSize(TypedValue.COMPLEX_UNIT_DIP,14);

        }




        Button envoi = (Button) findViewById(R.id.newRecordButton);
        envoi.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                ConnectivityManager cm =
                        (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();

                if (isConnected) {

                    if (hasMail) {

                        Intent myIntent = new Intent(MainActivity.this, GeoLocActivity.class);
                        MainActivity.this.startActivity(myIntent);
                    } else {
                        Toast.makeText(context, "Veuillez renseigner un mail de suivi", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(context, "Aucune connection internet n'est disponible ...", Toast.LENGTH_LONG).show();
                }

            }
        });


        Button sendSaved = (Button) findViewById(R.id.callServiceButton);
        sendSaved.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {


                if (ContextCompat.checkSelfPermission(context,
                        android.Manifest.permission.CALL_PHONE)
                        == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"+R.string.texteTelephoneAppelUrgenceVelo));
                    startActivity(callIntent);
                }else{
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{android.Manifest.permission.CALL_PHONE},
                            MY_PERMISSIONS_REQUEST_CALL_PHONE);

                }

            }
        });





    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE : {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"+R.string.texteTelephoneAppelUrgenceVelo));
                    startActivity(callIntent);

                } else {

                    Toast.makeText(context, "Vous n'avez pas autorisé l'utilisation du tel", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    public void onResume() {
        super.onResume();
        VelobsSingleton.getInstance().reset();

        Button mailButton = (Button) findViewById(R.id.mailButton);
        mailButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Intent myIntent = new Intent(MainActivity.this, MailActivity.class);
                MainActivity.this.startActivity(myIntent);

            }
        });

        TextView tvmail = (TextView) findViewById(R.id.mailtv) ;
        String mail = getValue(this) ;
        if (mail!=null) {
            tvmail.setText("Mail de suivi : "+mail);
            mailButton.setText("Changer le mail de suivi");
            VelobsSingleton.getInstance().mail = mail ;
            hasMail = true ;
        } else {
            tvmail.setText("Vous n'avez pas de mail de suivi");
            mailButton.setText("Ajouter un mail de suivi");
            hasMail = false ;
        }

    }


    public static final String PREFS_NAME = "VelobsPrefs";
    public static final String PREFS_KEY = "mail_String";

    public String getValue(Context context) {
        SharedPreferences settings;
        String text;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); 
        text = settings.getString(PREFS_KEY, null); 
        return text;
    }

    public void save(Context context, String text) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); 
        editor = settings.edit(); 

        editor.putString(PREFS_KEY, text); 
        editor.commit(); 
    }

}
